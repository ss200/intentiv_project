package com.intentiv_samet_dundar.model.response

data class PendingBalance(
    val currency: String,
    var value: String
)