package com.intentiv_samet_dundar.model.response

data class BottomSheetItem(
    var name: String,
    var value: String
)