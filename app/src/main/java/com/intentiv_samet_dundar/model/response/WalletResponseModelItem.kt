package com.intentiv_samet_dundar.model.response

data class WalletResponseModelItem(
    var balance: Balance,
    var cvv: String,
    val image: String,
    var number: String,
    var pendingBalance: PendingBalance,
    var isSelected : Boolean = false
)