package com.intentiv_samet_dundar.model.response

data class Balance(
    val currency: String,
    var value: String
)