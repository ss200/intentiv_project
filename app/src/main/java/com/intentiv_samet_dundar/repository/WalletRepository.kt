package com.intentiv_samet_dundar.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import com.intentiv_samet_dundar.model.response.WalletResponseModelItem
import com.intentiv_samet_dundar.repository.localDataSource.WalletLocalDataSource
import java.lang.Exception


class WalletRepository(
//    private val remoteDataSource: RemoteDataSource,
    private val walletLocalDataSource: WalletLocalDataSource
) : Repository {

    override fun fetchWallet(): LiveData<Result<List<WalletResponseModelItem>>> {
        Result.Loading
        try {
            return walletLocalDataSource.fetchWalletResponseModel().map {
                Result.Success(it)
            }
        } catch (e: Exception) {
            return walletLocalDataSource.fetchWalletResponseModel().map {
                Result.Error(e)
            }
        }

    }


}