package com.intentiv_samet_dundar.repository

import androidx.lifecycle.LiveData
import com.intentiv_samet_dundar.model.response.WalletResponseModelItem

interface Repository {

    fun fetchWallet(): LiveData<Result<List<WalletResponseModelItem>>>
}