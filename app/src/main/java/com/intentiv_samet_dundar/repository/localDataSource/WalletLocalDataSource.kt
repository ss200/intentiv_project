package com.intentiv_samet_dundar.repository.localDataSource

import android.content.Context
import android.util.Log
import com.intentiv_samet_dundar.R
import com.intentiv_samet_dundar.model.response.WalletResponseModel
import com.intentiv_samet_dundar.repository.WalletDataSource
import com.intentiv_samet_dundar.utils.SingleLiveEvent
import com.intentiv_samet_dundar.utils.convertModel
import java.io.*

class WalletLocalDataSource(
    private val context: Context
) : WalletDataSource {

    private var walletResponseModel = SingleLiveEvent<WalletResponseModel>()

    fun fetchWalletResponseModel(): SingleLiveEvent<WalletResponseModel> {
        getRawJson()
        return walletResponseModel
    }

    fun getRawJson() {
        val resource = context.resources.openRawResource(R.raw.wallet)
        val writer: Writer = StringWriter()
        val buffer = CharArray(1024)
        try {
            val reader: Reader = BufferedReader(InputStreamReader(resource, "UTF-8"))
            var n: Int
            while (reader.read(buffer).also { n = it } != -1) {
                writer.write(buffer, 0, n)
            }
        } finally {
            resource.close()
        }
        val jsonString = writer.toString()

        jsonString?.let {

            walletResponseModel.postValue(convertModel(it, WalletResponseModel::class.java))

        }
        Log.d("sametsametsamet", jsonString)
    }

}