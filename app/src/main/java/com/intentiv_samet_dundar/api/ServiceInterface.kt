package com.intentiv_samet_dundar.api

import com.intentiv_samet_dundar.model.response.WalletResponseModel
import retrofit2.http.*

interface ServiceInterface {

    @GET("wallets")
    suspend fun wallet(): WalletResponseModel

}