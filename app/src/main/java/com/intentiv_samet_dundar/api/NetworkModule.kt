package com.intentiv_samet_dundar.api

import android.annotation.SuppressLint
import com.blankj.utilcode.BuildConfig
import com.blankj.utilcode.util.SPUtils
import com.intentiv_samet_dundar.utils.BASE_URL
import com.intentiv_samet_dundar.utils.CustomHttpLogger
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

class NetworkModule {

    fun service(): ServiceInterface {

        val httpClient = getHttpClient()
        //httpClient.authenticator(TokenAuthenticator())

        val clt = httpClient.build()

        if (SPUtils.getInstance().getString("BASE_URL").isNullOrEmpty().not())
            BASE_URL = SPUtils.getInstance().getString("BASE_URL")

        val retrofit = getRetrofitBuilder(clt).build()

        return retrofit.create(ServiceInterface::class.java)
    }
}

fun getUnsafeOkHttpClient(builder: OkHttpClient.Builder): OkHttpClient.Builder {
    try {
        // Create a trust manager that does not validate certificate chains
        val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
            @SuppressLint("TrustAllX509TrustManager")
            @Throws(CertificateException::class)
            override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {
            }

            @SuppressLint("TrustAllX509TrustManager")
            @Throws(CertificateException::class)
            override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {
            }

            override fun getAcceptedIssuers(): Array<X509Certificate> {
                return arrayOf()
            }
        })

        // Install the all-trusting trust manager
        val sslContext = SSLContext.getInstance("SSL")
        sslContext.init(null, trustAllCerts, java.security.SecureRandom())

        // Create an ssl socket factory with our all-trusting manager
        val sslSocketFactory = sslContext.socketFactory

        //val builder = OkHttpClient.Builder()
        builder.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
        builder.hostnameVerifier({ hostname, session -> true })
        return builder
    } catch (e: Exception) {
        e.printStackTrace()
        throw RuntimeException(e)
    }

}

fun getHttpClient(addAccessToken: Boolean = true): OkHttpClient.Builder {
    val logging = HttpLoggingInterceptor(CustomHttpLogger())
    if (BuildConfig.DEBUG) {
        logging.level = HttpLoggingInterceptor.Level.BODY
    } else {
        logging.level = HttpLoggingInterceptor.Level.BODY
    }
    val httpClient = getUnsafeOkHttpClient(OkHttpClient.Builder().apply {
        connectTimeout(1, TimeUnit.HOURS)
        writeTimeout(1, TimeUnit.HOURS)
        readTimeout(1, TimeUnit.HOURS)
        callTimeout(1, TimeUnit.HOURS)
    })
    httpClient.addInterceptor(logging)
    httpClient.addInterceptor { chain ->
        val original = chain.request()
        val request = original.newBuilder()
            .header("Content-Type", "application/json; charset=utf-8")
//        if (addAccessToken) {
//            request.header("AppId", DeviceUtils.getAndroidID())
//            request.header("ClientVersion", BuildConfig.VERSION_NAME)
//            request.header("ClientBuildVersion", BuildConfig.VERSION_CODE.toString())
//            request.header("DeviceType", "Android")
//            request.header("OSVersion", DeviceUtils.getSDKVersionCode().toString())
//            request.header("Access-Control-Allow-Origin", "*")
//        }

        chain.proceed(request.build())
    }
    return httpClient
}

fun getRetrofitBuilder(client: OkHttpClient): Retrofit.Builder {
    return Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(BASE_URL)
        .client(client)
}