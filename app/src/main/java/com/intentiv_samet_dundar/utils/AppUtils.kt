package com.intentiv_samet_dundar.utils

import androidx.lifecycle.MutableLiveData
import com.google.gson.GsonBuilder
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import okhttp3.ResponseBody
import retrofit2.HttpException
import retrofit2.Response
import java.net.UnknownHostException
import java.util.*

fun <T> convertModel(payload: String, clazz: Class<T>): T {
    val builder = GsonBuilder().disableHtmlEscaping()
    val gson = builder.create()
    var model = gson.fromJson(payload, clazz)
    return model
}

fun convertJson(requestModel: Any): String? {
    val builder = GsonBuilder().disableHtmlEscaping()
    val gson = builder.create()
    val json = gson.toJson(requestModel)
    return json
}
