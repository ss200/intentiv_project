package com.intentiv_samet_dundar.utils

import android.text.Editable
import android.text.TextWatcher
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.aminography.redirectglide.GlideApp
import com.aminography.redirectglide.RedirectGlideUrl
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.intentiv_samet_dundar.R


fun TextView.txtColor(color: Int) {
    this.setTextColor(ContextCompat.getColor(this.context, color))
}

@Suppress("DeprecatedCallableAddReplaceWith")
@Deprecated("using for setImageResource")
fun ImageView.loadImageDrawable(drawableRes: Int) {
    Glide.with(this.context)
        .load(drawableRes)
        .apply(
            RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
        )
        .error(R.drawable.ic_launcher_background)
        .into(this)

}

fun ImageView.loadImage(url: String?) {
    url?.let {
        Glide.with(this.context)
            .load(url)
            .apply(
                RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            )
            .error(R.drawable.ic_launcher_background)
            .into(this)
    }

}

fun ImageView.loadRedirect(url: String?) {
    url?.let {
        GlideApp.with(context)
            .load(RedirectGlideUrl(url, 10))
            .into(this)
    }

}


fun ImageView.loadRoundedImage(imageUrl: String?) {

    imageUrl?.let {
        if (it.isNotEmpty()) {
            Glide.with(context)
                .load(it)
                .apply(
                    RequestOptions.centerInsideTransform().transform(
                        CenterCrop(),
                        GranularRoundedCorners(14f, 14f, 14f, 14f)
                    )
                )
                .error(R.drawable.ic_launcher_background)
                .into(this)
        }
    }

}

fun ImageView.loadCircleImage(imageUrl: String?) {
    imageUrl?.let {
        Glide.with(context)
            .load(it)
            .apply(
                RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .circleCrop()
            )
            .error(R.drawable.ic_launcher_background)
            .into(this)
    }

}

fun TextInputEditText.clearInputError(
    tilInput: TextInputLayout) {
    addTextChangedListener(object : TextWatcher {
        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            tilInput.isErrorEnabled = false
        }

        override fun beforeTextChanged(
            s: CharSequence, start: Int, count: Int,
            after: Int,
        ) {
        }

        override fun afterTextChanged(s: Editable) {}
    })
}