package com.intentiv_samet_dundar.di

import com.intentiv_samet_dundar.api.NetworkModule
import com.intentiv_samet_dundar.repository.WalletRepository
import com.intentiv_samet_dundar.repository.localDataSource.WalletLocalDataSource
import com.intentiv_samet_dundar.viewModel.MainViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    single { WalletLocalDataSource(androidContext()) }
    single { WalletRepository(get()) }
    single { NetworkModule() }
}


val viewModelModule = module {
    viewModel { MainViewModel(get(),get()) }
}
