package com.intentiv_samet_dundar.ui.wallet

import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import androidx.viewpager2.widget.ViewPager2
import com.blankj.utilcode.util.LogUtils
import com.github.nitrico.lastadapter.BR
import com.github.nitrico.lastadapter.LastAdapter
import com.github.nitrico.lastadapter.Type
import com.intentiv_samet_dundar.R
import com.intentiv_samet_dundar.base.BaseFragment
import com.intentiv_samet_dundar.databinding.FragmentWalletBinding
import com.intentiv_samet_dundar.databinding.ItemCardLayoutBinding
import com.intentiv_samet_dundar.model.response.WalletResponseModelItem
import com.intentiv_samet_dundar.repository.Result
import com.intentiv_samet_dundar.ui.bottomSheet.BottomSheetFragment
import com.intentiv_samet_dundar.utils.loadImage
import com.intentiv_samet_dundar.utils.setGone
import com.intentiv_samet_dundar.viewModel.MainViewModel
import com.murgupluoglu.request.STATUS_ERROR
import com.murgupluoglu.request.STATUS_LOADING
import com.murgupluoglu.request.STATUS_SUCCESS
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class WalletFragment : BaseFragment<FragmentWalletBinding>(),
    BottomSheetFragment.BottomSheetListener {

    private val sharedViewModel: MainViewModel by sharedViewModel()

    lateinit var adapter: LastAdapter
    private var countUiUpTimer: CountDownTimer? = null

    override fun getLayoutId(): Int = R.layout.fragment_wallet

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObserverLocale()
        initObserverRemote()
        initAdapter()
        initListener()
        sharedViewModel.requestWalletResponse()
    }

    private fun initObserverRemote() {
        sharedViewModel.walletResponseModel.observe(viewLifecycleOwner) {
            when (it.status) {
                STATUS_LOADING -> {
                    showProgressDialog()
                }
                STATUS_SUCCESS -> {
                    hideProgressDialog()
                    hideProgressDialog()
                    sharedViewModel.walletList.clear()
                    it.responseObject?.let { wallets -> sharedViewModel.walletList.addAll(wallets) }
                    sharedViewModel.walletList[0].isSelected = true
                    initUi()
                    adapter.notifyDataSetChanged()
                }
                STATUS_ERROR -> {
                    hideProgressDialog()
                }
            }
        }
    }

    fun initListener() {
        binding.refreshImageView.setOnClickListener {
            BottomSheetFragment.instance.apply {
                //iniliaze fun
            }.also {
                it.show(parentFragmentManager, "bottomsheet")
                it.sheetItem.clear()
                it.sheetItem.add(sharedViewModel.getSelectedItem()!!)
                it.listener = this
            }
        }

    }

    fun initObserverLocale() {
        showProgressDialog()
        sharedViewModel.wallet.observe(viewLifecycleOwner) { response ->
            LogUtils.d("sametsametsamet", response)
            when (response) {
                is Result.Success -> {

                }
                is Result.Error -> {
                    hideProgressDialog()
                }
                is Result.Loading -> {
                    showProgressDialog()
                }
                else -> {}
            }
        }
    }

    private fun initUi() {
        countTimer()
        var model = sharedViewModel.getSelectedItem()

        model?.apply {
            binding.priceTextView.text = balance.currency + balance.value
            binding.pendingBalanceTextView.text = pendingBalance.currency + pendingBalance.value
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        countUiUpTimer?.cancel()
    }

    fun countTimer() {
        countUiUpTimer?.cancel()
        var i = 0
        countUiUpTimer = object : CountDownTimer(1000, 1000) {
            override fun onTick(p0: Long) {
                binding.secondTextView.text = i++.toString()

            }

            override fun onFinish() {
                this.start()
            }
        }.start()
    }

    private fun initAdapter() {
        adapter =
            LastAdapter(
                sharedViewModel.walletList, BR.item
            ).map<WalletResponseModelItem>(Type<ItemCardLayoutBinding>(R.layout.item_card_layout).onBind { holder ->
                val data = holder.binding.item
                data?.let { item ->
                    holder.binding.cardImageView.loadImage(item.image)
                    holder.binding.cardNumberTextView.text = item.number
                    holder.binding.cardCvv.text = item.cvv
                }
            })
        binding.viewpager.adapter = adapter
        binding.indicator.setViewPager2(binding.viewpager)
        if (sharedViewModel.walletList.size == 1) {
            binding.indicator.setGone()
        }

        binding.viewpager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                sharedViewModel.clearList()
                sharedViewModel.walletList[position].isSelected = true
                initUi()
            }

            override fun onPageScrollStateChanged(state: Int) {
            }

        })
    }

    override fun bottomSheetItemClick(item: Any, layoutPosition: Int) {
        when (item) {
            is WalletResponseModelItem -> {
                LogUtils.d("samsamsam", item.number)
                LogUtils.d("samsamsam", item.cvv)
                sharedViewModel.getSelectedItem()?.number = item.number
                sharedViewModel.getSelectedItem()?.cvv = item.cvv
                sharedViewModel.getSelectedItem()?.balance?.value = item.balance.value
                sharedViewModel.getSelectedItem()?.pendingBalance?.value = item.pendingBalance.value
                adapter.notifyDataSetChanged()
                initUi()
            }
            else -> {}
        }

    }


}