package com.intentiv_samet_dundar.ui.saleAppoint

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.intentiv_samet_dundar.R
import com.intentiv_samet_dundar.base.BaseFragment
import com.intentiv_samet_dundar.databinding.FragmentSaleAppointBinding

class SaleAppointFragment : BaseFragment<FragmentSaleAppointBinding>() {

    override fun getLayoutId(): Int = R.layout.fragment_sale_appoint

}