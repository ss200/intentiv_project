package com.intentiv_samet_dundar.ui.bottomSheet

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.blankj.utilcode.util.LogUtils
import com.github.nitrico.lastadapter.BR
import com.github.nitrico.lastadapter.LastAdapter
import com.github.nitrico.lastadapter.Type
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.button.MaterialButton
import com.intentiv_samet_dundar.R
import com.intentiv_samet_dundar.databinding.ItemCardLayoutBinding
import com.intentiv_samet_dundar.databinding.ItemEditLayoutBinding
import com.intentiv_samet_dundar.model.response.BottomSheetItem
import com.intentiv_samet_dundar.model.response.WalletResponseModelItem

class BottomSheetFragment : BottomSheetDialogFragment() {

    lateinit var adapter: LastAdapter

    var sheetItem = arrayListOf<Any>()
    var listener: BottomSheetListener? = null


    companion object {
        val instance: BottomSheetFragment
            get() = BottomSheetFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bottom_sheet, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
    }


    fun initAdapter() {
        adapter =
            view?.findViewById<RecyclerView>(R.id.bottomSheetRecyclerView)?.let { recyclerView ->
                LastAdapter(sheetItem, BR.item)
                    .map<WalletResponseModelItem>(
                        Type<ItemEditLayoutBinding>(R.layout.item_edit_layout).onBind { holder ->
                            val data = holder.binding.item
                            data?.let { item ->

                                holder.binding.button.setOnClickListener {
                                    item.number = holder.binding.cardNumberTextView.text.toString()
                                    item.cvv = holder.binding.cardCvv.text.toString()
                                    item.balance.value = holder.binding.cardBalance.text.toString()
                                    item.pendingBalance.value = holder.binding.cardPendingBalance.text.toString()
                                    listener?.bottomSheetItemClick(item,holder.bindingAdapterPosition)
                                    dismiss()
                                }

                            }


                        })

                    .into(recyclerView)
            }!!

    }

    interface BottomSheetListener {
        fun bottomSheetItemClick(item: Any, layoutPosition: Int)
    }

}