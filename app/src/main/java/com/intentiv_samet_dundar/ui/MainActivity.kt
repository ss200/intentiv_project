package com.intentiv_samet_dundar.ui

import android.os.Bundle
import android.os.Environment
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.blankj.utilcode.util.LogUtils
import com.intentiv_samet_dundar.R
import com.intentiv_samet_dundar.base.BaseActivity
import com.intentiv_samet_dundar.databinding.ActivityMainBinding
import com.intentiv_samet_dundar.model.response.WalletResponseModelItem
import com.intentiv_samet_dundar.repository.Result
import com.intentiv_samet_dundar.viewModel.MainViewModel
import org.json.JSONArray
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.*


class MainActivity : BaseActivity<ActivityMainBinding>() {

    private lateinit var navController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration

    private val viewModel: MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        initNavigation()
        window.statusBarColor = ContextCompat.getColor(this, R.color.multinet_color)
    }

    private fun initNavigation() {
        val navHostFragment = supportFragmentManager.findFragmentById(
            R.id.navHostFragment
        ) as NavHostFragment
        navController = navHostFragment.navController
        // Setup the bottom navigation view with navController
        binding.bottomNavigation.setupWithNavController(navController)
        // Setup the ActionBar with navController and 3 top level destinations
        appBarConfiguration = AppBarConfiguration(
            setOf(R.id.fragment_wallet, R.id.fragment_sale_aapoint)
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
    }

    override fun getLayoutId(): Int = R.layout.activity_main
}