package com.intentiv_samet_dundar.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.intentiv_samet_dundar.api.NetworkModule
import com.intentiv_samet_dundar.model.response.WalletResponseModel
import com.intentiv_samet_dundar.model.response.WalletResponseModelItem
import com.intentiv_samet_dundar.repository.Result
import com.intentiv_samet_dundar.repository.WalletRepository
import com.intentiv_samet_dundar.utils.SingleLiveEvent
import com.murgupluoglu.request.RESPONSE
import com.murgupluoglu.request.request

class MainViewModel(private val walletRepository: WalletRepository,
private val networkModule: NetworkModule) : ViewModel() {

    val _wallets : LiveData<Result<List<WalletResponseModelItem>>> =
        walletRepository.fetchWallet()

    val wallet: LiveData<Result<List<WalletResponseModelItem>>> = _wallets

    val walletList = arrayListOf<WalletResponseModelItem>()

    var walletResponseModel = SingleLiveEvent<RESPONSE<WalletResponseModel>>()

    fun requestWalletResponse(){
        walletResponseModel.request(
            viewModelScope = viewModelScope,
            suspendfun = {
                networkModule.service().wallet()
            }
        )
    }

    fun clearList() {
        walletList.forEach {
            it.isSelected = false
        }
    }

    fun getSelectedItem(): WalletResponseModelItem? {

        return walletList.find { it.isSelected }
    }
}